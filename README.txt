Snake
============================================
This repository contains my work for my AP Comp Sci Project: Snake

Version 1.2
File Type: Jar
Java Version Needed: 8.0

Features:
	Graphic User Interface
	Different titles for both Gameplay and Game Over
	Increasing difficulty as game goes on
	Scoreboard

@TODO: Next releases
	High Scores List
	Menu with Buttons - paths to directions, High Scores and gameplay
	Themes
	
Directions:
	After downloading the repository as a .zip
	Extract all Files
	Open the repository's folder
	Double-click and run Snake.exe
